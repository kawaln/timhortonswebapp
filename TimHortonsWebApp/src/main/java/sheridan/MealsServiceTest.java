package sheridan;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import sheridan.MealType;

public class MealsServiceTest {

	@Test
	public void testDrinksRegular() {
		 
        List<String> stuff  = MealsService.getAvailableMealTypes(MealType.DRINKS);
		
       
		assertTrue("Invalid valid Drink", stuff != null );
	
	}

	@Test
	public void testDrinksException() {
    List<String> stuff  = MealsService.getAvailableMealTypes(null);
	assertNotEquals("Invalid value for Drink", stuff.get(0));
	}
	
	@Test
	public void testDrinksBoundaryIn() {
    List<String> stuff  = MealsService.getAvailableMealTypes(MealType.DRINKS);
	 
     assertTrue("Invalid value for Drink", stuff.size() > 3);
	}

	@Test
	public void testDrinksBoundaryOut() {
	    List<String> stuff  = MealsService.getAvailableMealTypes(null);

	    assertFalse("Invalid value for Drink", stuff.size() < 1);
	}

}
